$(window).load(function(){
	fPlay();
});
$(function(){
	$(".lists .love").on('click',function(){
		if($(this).find('img').attr('src')=='css/images/loveGray.png'){
			$(this).find('img').attr("src","css/images/loveRed.png");
		}else{
			$(this).find('img').attr("src","css/images/loveGray.png");
		}
	});
	/*点击列表播放按钮*/
	$(".lists .start em").click(function(){
		/*开始放歌*/
		var sid=$(this).attr("sonN");
		songIndex=sid;
		$("#audio").attr("src",'songs/'+sid+'.mp3');
		audio=document.getElementById("audio");//获得音频元素
		/*显示歌曲总长度*/
		if(audio.paused){
			audio.play();
		}
		else
			audio.pause();
		audio.addEventListener('timeupdate',updateProgress,false);

		audio.addEventListener('play',audioPlay,false);
		audio.addEventListener('pause',audioPause,false);
		audio.addEventListener('ended',audioEnded,false);
		//对audio元素监听pause事件
		/*外观改变*/
		$(".lists  .start em").css({
			"background":"",
			"color":""
		});
		$(this).css({
			"background":'url("css/images/T1X4JEFq8qXXXaYEA_-11-12.gif") no-repeat',
			"color":"transparent"
		});

		/*底部显示歌曲信息*/
		var songName=$(this).parent().parent().parent().parent().find(".colsn").html();
		var singerName =$(this).parent().parent().parent().parent().find(".colcn").html();
		$(".songName").html(songName);
		$(".songPlayer").html(singerName);
	});
	/*双击播放*/
	$(".lists  .songList").dblclick(function(){
		var sid = $(this).find(".lists  .start em").html();
		$(".lists  .start em[sonN="+sid+"]").click();
	});
	/*底部进度条控制*/
	$( ".lists  .dian" ).draggable({
		containment:".pro2",
		drag: function() {
			var l=$(".dian").css("left");
			var le = parseInt(l);
			audio.currentTime = audio.duration*(le/678);
		}
	});
	/*音量控制*/
	$( ".lists  .dian2" ).draggable({
		containment:".volControl",
		drag: function() {
			var l=$(".dian2").css("left");
			var le = parseInt(l);
			audio.volume=(le/80);
		}
	});
	/*底部播放按钮*/
	$(".lists  .playBtn").click(function(){
		var p = $(this).attr("isplay");
		if (p==0) {
			$(this).css("background","url('css/images/pauseBtn.png')");
			$(this).css("background-size","cover");
			$(this).attr("isplay","1");
		};
		if (p==1) {
			$(this).css("background","url('css/images/startBtn.png')");
			$(this).attr("isplay","0");
		};
		if(audio.paused)
			audio.play();
		else
			audio.pause();

	});
	$(".lists  .mode").click(function(){

	});
	/*切歌*/
	$(".lists  .prevBtn").click(function(){
		var sid = parseInt(songIndex)-1;
		$(".start em[sonN="+sid+"]").click();
	});
	$(".lists  .nextBtn").click(function(){
		var sid = parseInt(songIndex)+1;
		$(".start em[sonN="+sid+"]").click();
	});

});
function calcTime(time){
	var hour;
	var minute;
	var second;
	hour = String ( parseInt ( time / 3600 , 10 ));
	if (hour.length ==1 )   hour='0'+hour;
	minute=String(parseInt((time%3600)/60,10));
	if(minute.length==1) minute='0'+minute;
	second=String(parseInt(time%60,10));
	if(second.length==1)second='0'+second;
	return minute+":"+second;
}
function updateProgress(ev){
	/*显示歌曲总长度*/
	var songTime = calcTime(Math.floor(audio.duration));
	$(".lists  .duration").html(songTime);
	/*显示歌曲当前时间*/
	var curTime = calcTime(Math.floor(audio.currentTime));
	$(".lists  .position").html(curTime);
	/*进度条*/
	var lef = 400*(Math.floor(audio.currentTime)/Math.floor(audio.duration));
	var llef = Math.floor(lef).toString()+"px";
	$(".lists  .dian").css("left",llef);
}
function audioPlay(ev){
	$(".lists  .playBtn").css("background","url('css/images/pauseBtn.png')");
	$(".lists  .playBtn").css("background-size","cover");
	$(".lists  .playBtn").attr("isplay","1");
}
function audioPause(ev){
	/* $(".start em").css({
	 "background":'url("css/images/pause.png") no-repeat 50% 50%',
	 "color":"transparent"
	 });*/
}
function audioEnded(ev){
	var sid = parseInt(songIndex)+1;
	$(".lists  .start em[sonN="+sid+"]").click();
}

var songIndex=0;
function mPlay()//开始播放
{
	var ms =audio.currentTime;
	show(ms);
	window.setTimeout("mPlay()",100)
}
function fPlay(){
	$(".lists  .start em[sonN="+songIndex+"]").click();
} 
